<?php
namespace Yusi\Gii\Model;

use Yii;
use yii\helpers\Inflector;
use yii\gii\CodeFile;

class Generator extends \yii\gii\generators\model\Generator {

    public $helperClass = 'common\components\Helper';

    public function rules() {
      $rules = parent::rules();
      $rules[] = ['helperClass', 'safe'];

      return $rules;
    }

    public function formView() {
      return Yii::getAlias('@vendor/yiisoft/yii2-gii/src/generators/model/form.php');
    }

    public function defaultTemplate() {
      return Yii::getAlias('@vendor/yiisoft/yii2-gii/src/generators/crud/default');
    }

    /**
     * {@inheritdoc}
     */
    public function generateLabels($table) {
        $labels = [];
        foreach ($table->columns as $column) {
            if ($this->generateLabelsFromComments && !empty($column->comment)) {
                $labels[$column->name] = $column->comment;
            } elseif (!strcasecmp($column->name, 'id')) {
                $labels[$column->name] = 'ID';
            } else {
                $label = Inflector::camel2words($column->name);
                if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                    $label = substr($label, 0, -3);
                }
                $labels[$column->name] = $label;
            }
        }

        return $labels;
    }

    public function afterValidate() {
      parent::afterValidate();
      $this->setHelperClass();
    }

    protected function setHelperClass() {
      try {
        Yii::getAlias('@common');
      } catch (\Exception $ex) {
        $this->helperClass = 'app\components\Helper';
      }
    }

    public function generate(): array {
        $files = parent::generate();
        $files[] = $this->getHelperFile();

        return $files;
    }

    protected function getHelperFile() {
        list($namespace) = explode('\\', $this->helperClass, 2);

        $path = Yii::getAlias("@$namespace/components/Helper.php");
        $file = new CodeFile($path, $this->render('../Helper.php', ['namespace' => $namespace]));

        return $file;
    }
}
