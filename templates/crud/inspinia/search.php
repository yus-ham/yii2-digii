<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */
use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator Digitak\Gii\Crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

echo "<?php\n";
?>
namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use <?= ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;

/**
 * <?= $searchModelClass ?> represents the model behind the search form of `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass ?> {

    protected $dp;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            <?= implode(",\n            ", $rules) ?>,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName='')
    {
        $query = <?= isset($modelAlias) ? $modelAlias : $modelClass ?>::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, $formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $this->dp = $dataProvider;
        }

        // grid filtering conditions
        <?= implode("\n        ", $searchConditions) ?>

        return $this->dp = $dataProvider;
    }
<?php foreach ($generator->selectedColumns as $columnName => $selected):
  if (!StringHelper::endsWith($columnName, '_id')) {
    continue;
  }
  ?>

    private $<?= $var = Inflector::variablize(Inflector::titleize($columnName)) .'s' ?>;

    public function get<?= $class = Inflector::classify(Inflector::titleize($columnName)) ?>() {
      if (!$this-><?= $var ?>) {
        $<?= $localVar = Inflector::variablize($columnName) .'s' ?> = ArrayHelper::map($this->dp->models, '<?= $columnName ?>', '<?= $columnName ?>');
        //$models = <?= $class ?>::find()->select('id,name')->andWhere(['id' => $<?= $localVar ?>])->all();
        //$this-><?= $var ?> = ArrayHelper::map($models, 'id', 'name');
      }
      return @$this-><?= $var ?>[$this-><?= $columnName ?>];
    }
<?php endforeach ?>
}
