<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator Digitak\Gii\Crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?php $generator->enablePjax && print 'use yii\widgets\Pjax;' ?>

/* @var $this yii\web\View */
<?php $generator->searchModelClass && print '/* @var $searchModel ' . ltrim($generator->searchModelClass, '\\') . " */\n" ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;

\yii\helpers\Url::remember();
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index row">
  <div class="col-lg-12">

    <div class="ibox">
      <div class="ibox-title">
            <h5><<?php ?>?= Html::encode($this->title) ?></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="fullscreen-link">
                    <i class="fa fa-expand"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
      </div>

      <div class="ibox-content">
        <div class="button-actions">
          <?= "<?= " ?>Html::a('<i class="fa fa-plus"></i> Tambah', ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="table-responsive">
   
<?php $generator->enablePjax and print "          <?php Pjax::begin() ?>\n" ?>
<?php if (!empty($generator->searchModelClass)): ?>
            <?= '<?php '. ($generator->indexWidgetType === 'grid' ? '// ' : null) ?>echo $this->render('_search', ['model' => $searchModel]) ?>
<?php endif ?>

<?php if ($generator->indexWidgetType === 'grid') { ?>
          <<?php ?>?= GridView::widget([
              'dataProvider' => $dataProvider,
<?php $generator->searchModelClass && print "              'filterModel' => \$searchModel,\n" ?>
              'columns' => [
<?php   foreach ($generator->selectedColumns as $columnName => $selected) {
          if (!$selected) {
            continue;
          }
          $lines = [];
          $templateLines = explode(PHP_EOL, $generator->attributeConfigTemplates[$columnName]);
          foreach ($templateLines as $line) {
            $lines[] = str_repeat(" ", 16) . $line;
          }
          echo implode(PHP_EOL, $lines) . chr(44) . PHP_EOL;
        } ?>
              ],
          ]) ?>
<?php } else { ?>
                <<?php ?>?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                    },
          ]) ?>
<?php } ?>
<?php $generator->enablePjax and print "        <?php Pjax::end() ?>\n" ?>

        </div>
      </div>
    </div>

  </div>
</div>
