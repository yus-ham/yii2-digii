<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator Digitak\Gii\Crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
\yii\helpers\Url::remember();
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view row">

  <div class="col-lg-12">
    <div class="ibox">
        <div class="ibox-title">
            <h5><?= "<?= " ?>Html::encode($this->title) ?></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="fullscreen-link">
                    <i class="fa fa-expand"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

            <div class="button-actions">
                <?= "<?= " ?>Html::a(<?= $generator->generateString('{icon} Daftar', ['icon' => '<i class="fa fa-list-alt"></i>']) ?>, ['index', <?= $urlParams ?>], ['class' => 'btn btn-info btn-extend be-left']) ?>
                <?= "<?= " ?>Html::a(<?= $generator->generateString('{icon} Update', ['icon' => '<i class="fa fa-edit"></i>']) ?>, ['update', <?= $urlParams ?>], ['class' => 'btn btn-warning btn-extend be-left']) ?>
                <?= "<?= " ?>Html::a(<?= $generator->generateString('{icon} Delete', ['icon' => '<i class="fa fa-trash"></i>']) ?>, ['delete', <?= $urlParams ?>], [
                    'class' => 'btn btn-danger btn-extend be-left',
                    'data' => [
                        'confirm' => <?= $generator->generateString('Are you sure you want to delete this item?') ?>,
                        'method' => 'post',
                    ],
                ]) ?>
                <?= "<?= " ?>Html::a(<?= $generator->generateString('{icon} Create', ['icon' => '<i class="fa fa-plus"></i>']) ?>, ['create', <?= $urlParams ?>], ['class' => 'btn btn-primary  btn-extend be-left']) ?>
            </div>

    <<?php ?>?= DetailView::widget([
        'model' => $model,
        'attributes' => [
<?php
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        echo "            '" . $name . "',\n";
    }
} else {
    foreach ($generator->getTableSchema()->columns as $column) {
        echo $generator->generateDetailViewAttributeConfig($column);
    }
}
?>
        ],
    ]) ?>
            
        </div>
      </div>
  </div>

</div>
