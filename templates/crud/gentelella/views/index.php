<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator Digitak\Gii\Crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?php $generator->enablePjax && print 'use yii\widgets\Pjax;' ?>

/* @var $this yii\web\View */
<?php $generator->searchModelClass && print '/* @var $searchModel ' . ltrim($generator->searchModelClass, '\\') . " */\n" ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;

\yii\helpers\Url::remember();
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

  <div class="x_panel">
    <div class="x_title">
      <h2><<?php ?>?= Html::encode($this->title) ?></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">

<?php $generator->enablePjax and print "      <?php Pjax::begin() ?>\n" ?>
<?php if (!empty($generator->searchModelClass)): ?>
      <?= '<?php '. ($generator->indexWidgetType === 'grid' ? '// ' : null) ?>echo $this->render('_search', ['model' => $searchModel]) ?>
<?php endif ?>

      <p class="text-muted font-13 m-b-30">
        <<?php ?>?= Html::a(<?= $generator->generateString('Tambah ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn btn-success']) ?>
      </p>
      <div class="table-responsive">
<?php if ($generator->indexWidgetType === 'grid') { ?>
        <<?php ?>?= GridView::widget([
            'dataProvider' => $dataProvider,
<?php $generator->searchModelClass && print "            'filterModel' => \$searchModel,\n" ?>
            'columns' => [
<?php   foreach ($generator->selectedColumns as $columnName => $selected) {
          if (!$selected) {
            continue;
          }
          $lines = [];
          $templateLines = explode(PHP_EOL, $generator->attributeConfigTemplates[$columnName]);
          foreach ($templateLines as $line) {
            $lines[] = str_repeat(" ", 14) . $line;
          }
          echo implode(PHP_EOL, $lines) . chr(44) . PHP_EOL;
        } ?>
            ],
        ]) ?>
<?php } else { ?>
                <<?php ?>?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                    },
                ]) ?>
<?php } ?>
      </div>
<?php $generator->enablePjax and print "      <?php Pjax::end() ?>\n" ?>

    </div>
  </div>

</div>
