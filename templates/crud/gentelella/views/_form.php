<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator Digitak\Gii\Crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
<?php $generator->hasColumnType('text') && print 'use dosamigos\tinymce\TinyMce;' ?>
<?php $generator->hasColumnType('FK') && print 'use kartik\select2\Select2;' ?>

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <<?php ?>?php $form = ActiveForm::begin([
      'layout'=>'horizontal',
      'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
          'label' => 'col-sm-3',
          'offset' => 'col-sm-offset-3',
          'wrapper' => 'col-sm-9',
          'error' => '',
          'hint' => '',
        ],
      ],
    ]) ?>

<?php
foreach ($generator->getColumnNames() as $attribute) {
  if (in_array($attribute, $generator->getDefaultSkippedColumns())) {
      continue;
  }
  if (!in_array($attribute, $safeAttributes)) {
      continue;
  }
  echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
} ?>
    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <<?php ?>?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
        <<?php ?>?= Html::a('<i class="fa fa-remove"></i> Cancel', Url::previous(), ['class' => 'btn btn-danger']) ?>
      </div>
    </div>

    <<?php ?>?php ActiveForm::end() ?>

</div>
