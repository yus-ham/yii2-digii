<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator Digitak\Gii\Crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?php $generator->enablePjax && print 'use yii\widgets\Pjax;' ?>

/* @var $this yii\web\View */
<?php $generator->searchModelClass && print '/* @var $searchModel ' . ltrim($generator->searchModelClass, '\\') . " */\n" ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;

\yii\helpers\Url::remember();
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
  <div class="magic-layout">
    <div id="panel2" class="panel panel-default magic-element width-full">

      <div class="panel-heading">
          <div class="panel-icon"><i class="icon ion-ios7-photos-outline"></i></div>
          <div class="panel-actions">
              <a role="button" data-refresh="#panel2" title="refresh" class="btn btn-sm btn-icon">
                  <i class="icon ion-refresh"></i>
              </a>
              <a role="button" data-expand="#panel2" title="expand" class="btn btn-sm btn-icon">
                  <i class="icon ion-arrow-resize"></i>
              </a>
              <a role="button" data-collapse="#panel2" title="collapse" class="btn btn-sm btn-icon">
                  <i class="icon ion-chevron-down"></i>
              </a>
              <a role="button" data-close="#panel2" title="close" class="btn btn-sm btn-icon">
                  <i class="icon ion-close-round"></i>
              </a>
          </div><!-- /panel-actions -->
          <h3 class="panel-title"><<?php ?>?= Html::encode($this->title) ?></h3>
      </div><!-- /panel-heading -->

      <div class="panel-body">
        <div class="project-index table-responsive">

<?php $generator->enablePjax and print "          <?php Pjax::begin() ?>\n" ?>
<?php if (!empty($generator->searchModelClass)): ?>
          <?= '<?php '. ($generator->indexWidgetType === 'grid' ? '// ' : null) ?>echo $this->render('_search', ['model' => $searchModel]) ?>
<?php endif ?>

<?php if ($generator->indexWidgetType === 'grid') { ?>
          <<?php ?>?= GridView::widget([
              'dataProvider' => $dataProvider,
<?php $generator->searchModelClass && print "              'filterModel' => \$searchModel,\n" ?>
              'columns' => [
<?php   foreach ($generator->selectedColumns as $columnName => $selected) {
          if (!$selected) {
            continue;
          }
          $lines = [];
          $templateLines = explode(PHP_EOL, $generator->attributeConfigTemplates[$columnName]);
          foreach ($templateLines as $line) {
            $lines[] = str_repeat(" ", 16) . $line;
          }
          echo implode(PHP_EOL, $lines) . chr(44) . PHP_EOL;
        } ?>
              ],
          ]) ?>
<?php } else { ?>
                <<?php ?>?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                    },
          ]) ?>
<?php } ?>
<?php $generator->enablePjax and print "          <?php Pjax::end() ?>\n" ?>
        </div>
      </div><!-- /panel-body -->

      <div class="panel-footer"></div>

    </div>
  </div>
</div>
