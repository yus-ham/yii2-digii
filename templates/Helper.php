<<?php ?>?php
namespace <?= $namespace ?>\components;

use Yii;

class Helper extends \yii\base\Component {

    public static function statusLabel($isActive, $bs4=false) {
      $class = $bs4 ? 'badge' : 'label';
      return $isActive
          ? "<span class='$class $class-success'>Aktif</span>"
          : "<span class='$class $class-danger'>Inaktif</span>";
    }

    public static function sqlDate($date=null) {
      if (!$date) {
        return date('Y-m-d H:i:s');
      }
      return date('Y-m-d H:i:s', is_numeric($date) ? $date : strtotime($date));
    }

    public static function rupiah($angka){
        $hasil_rupiah = number_format($angka,2,',','.');
        return $hasil_rupiah;
    }

    public static function listBulan()
    {
        $bulan = [
          '1'=>'Januari',
          '2'=>'Februari',
          '3'=>'Maret',
          '4'=>'April',
          '5'=>'Mei',
          '6'=>'Juni',
          '7'=>'Juli',
          '8'=>'Agustus',
          '9'=>'September',
          '10'=>'Oktober',
          '11'=>'November',
          '12'=>'Desember'
        ];

        return $bulan;
    }

    public static function listTahun()
    {
        $tahun = [];

        for($i = (date('Y')-5); $i < (date('Y')+5); $i++){
            $tahun[$i] = $i;
        }
        return $tahun;
    }



    public static function rp($jumlah,$null=null)
    {
        if($jumlah==null) {
            return $null;
        }

        if(is_numeric($jumlah)) {
            return number_format($jumlah,0,',','.');
        }

        return $jumlah;
    }

    public static function getTanggalBiasa($tanggal){
      if(!empty($tanggal)){
          $tgl_lama = explode('-',$tanggal);
          $tgl_baru = $tgl_lama['2'].'/'.$tgl_lama['1'].'/'.$tgl_lama['0'];
      }
      else{
          $tgl_baru = '';
      }
        return $tgl_baru;
    }

    public static function getTanggalSingkat($tanggal)
    {
        if($tanggal==null)
            return null;

        if($tanggal=='0000-00-00')
            return null;

        $time = strtotime($tanggal);

        return date('j',$time).' '.Helper::getBulanSingkat(date('m',$time)).' '.date('Y',$time);
    }

    public static function getTanggal($tanggal)
    {
        if($tanggal==null)
            return null;

        if($tanggal=='0000-00-00')
            return null;

        $time = strtotime($tanggal);

        return date('j',$time).' '.Helper::getBulanLengkap(date('m',$time)).' '.date('Y',$time);
    }

    public static function getBulanSingkat($i)
    {
        $bulan = '';

        if(strlen($i)==1) $i = '0'.$i;

        if($i=='01') $bulan = 'Jan';
        if($i=='02') $bulan = 'Feb';
        if($i=='03') $bulan = 'Mar';
        if($i=='04') $bulan = 'Apr';
        if($i=='05') $bulan = 'Mei';
        if($i=='06') $bulan = 'Jun';
        if($i=='07') $bulan = 'Jul';
        if($i=='08') $bulan = 'Agt';
        if($i=='09') $bulan = 'Sep';
        if($i=='10') $bulan = 'Okt';
        if($i=='11') $bulan = 'Nov';
        if($i=='12') $bulan = 'Des';

        return $bulan;

    }


    public static function getBulanLengkap($i)
    {
        $bulan = '';

        if(strlen($i)==1) $i = '0'.$i;

        if($i=='01') $bulan = 'Januari';
        if($i=='02') $bulan = 'Februari';
        if($i=='03') $bulan = 'Maret';
        if($i=='04') $bulan = 'April';
        if($i=='05') $bulan = 'Mei';
        if($i=='06') $bulan = 'Juni';
        if($i=='07') $bulan = 'Juli';
        if($i=='08') $bulan = 'Agustus';
        if($i=='09') $bulan = 'September';
        if($i=='10') $bulan = 'Oktober';
        if($i=='11') $bulan = 'November';
        if($i=='12') $bulan = 'Desember';

        return $bulan;

    }
    
    /**
     * @param \yii\web\UploadedFile $file no action if invalid
     * @param \yii\db\ActiveRecord $model
     */
    public static function uploadAttachment($directory, $file, $model, $attribute) {
        if (!$file) {
          return;
        }
        $filename = \sprintf('%s/%s_%s', $directory, uniqid(time(), true), $file->name);
        $cloudPath = "cloud:$filename";
        $oldModel = clone $model;

        if (Yii::$app->fs->copy("tmp:{$file->tempName}", $cloudPath)) {
          Yii::$app->fs->setVisibility($cloudPath, 'public');
          Yii::$app->params['fs_model'] = $oldModel;
          Yii::$app->fs->delete($oldModel->$attribute);
          Yii::$app->params['fs_model'] = $model;

          return $model->$attribute = $filename;
        }
    }

    public static function build_calendar($month,$year,$dateArray, $data = NULL) {

        // Create array containing abbreviations of days of week.
        $daysOfWeek = array('S', 'M','T','W','T','F','S');

        // What is the first day of the month in question?
        $firstDayOfMonth = mktime(0,0,0,$month,1,$year);

        // How many days does this month contain?
        $numberDays = date('t',$firstDayOfMonth);

        // Retrieve some information about the first day of the
        // month in question.
        $dateComponents = getdate($firstDayOfMonth);

        // What is the name of the month in question?
        $monthName = $dateComponents['month'];

        // What is the index value (0-6) of the first day of the
        // month in question.
        $dayOfWeek = $dateComponents['wday'];

        // Create the table tag opener and day headers

        $calendar = "<caption style='margin-left:50px;'><p  style='margin-left:1px; float:right; margin-right:50px;'><b> $monthName $year </b></p></caption>";
        $calendar .= "<div class='container  table-responsive'>";
        $calendar .= "<table border='1' class='calendar table' style='width:98%; margin-left:5px;'>";
        $calendar .= "<tr>";

        // Create the calendar headers

        foreach($daysOfWeek as $day) {
            $calendar .= "<th class='headers' style='height:103px; border:1px;'>$day</th>";
        } 

        // Create the rest of the calendar

        // Initiate the day counter, starting with the 1st.

        $currentDay = 1;

        $calendar .= "</tr><tr>";

        // The variable $dayOfWeek is used to
        // ensure that the calendar
        // display consists of exactly 7 columns.

        if ($dayOfWeek > 0) { 
            $calendar .= "<td colspan='$dayOfWeek'>&nbsp;</td>"; 
        }
        
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
    
        while ($currentDay <= $numberDays) {

            // Seventh column (Saturday) reached. Start a new row.

            if ($dayOfWeek == 7) {

                $dayOfWeek = 0;
                $calendar .= "</tr><tr>";

            }
            
            $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);
            
            $date = "$year-$month-$currentDayRel";
            $order = OrderHotelItem::find()
                    ->leftJoin('t_order', 't_order.id=t_order_hotel_item.order_id')
                    ->where(['t_order.status' => 2])->andwhere(['LIKE', 't_order_hotel_item.date', $date])->count();
            // var_dump($date);exit();
            $calendar .= "<td class='day' rel='$date' style='height:103px;'>$currentDay <br/>";
            if(!empty($order)){
                // foreach($order as $key => $value){
                    $calendar .= "<p style='float:right; bottom:0px;'>Booking : <b>".$order."</b></p><br/>";
                // }
            }
            $calendar .= "</td>";
            $calendar .= "</div>";
            // Increment counters
    
            $currentDay++;
            $dayOfWeek++;
        }

        // Complete the row of the last week in month, if necessary

        if ($dayOfWeek != 7) { 
        
            $remainingDays = 7 - $dayOfWeek;
            $calendar .= "<td colspan='$remainingDays' style='height:103px;'>&nbsp;</td>"; 

        }

        $calendar .= "</tr>";

        $calendar .= "</table>";

        return $calendar;

    } 

}
