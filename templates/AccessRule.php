<<?php ?>?php
namespace <?= $namespace ?>\components;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * ```php
 * 'access' => [
 *    'class' => \yii\filters\AccessControl::class,
 *    'ruleConfig' => ['class'=>\<?= $namespace ?>\components\AccessRule::class],
 *    'rules' => [ ... ],
 * ]
 * ```
 * rule examples
 * ```php
 *    'rules' => [ // hanya user login yang bisa akses
 *      ['allow'=>1, 'roles'=>['@']],
 *    ],
 *
 *    'rules' => [ // hanya admin ($identity->user_role_id = 1) yang bisa akses
 *      ['allow'=>1, 'roles'=>[1]],
 *    ],
 * ```
 */
class AccessRule extends \yii\filters\AccessRule {
  
  public $roleAttribute = 'user_role_id';

  protected function matchRole($user) {
    if (!$this->roles) {
      return true;
    }
    foreach ($this->roles ?: [] as $role) {
      if ($this->userHasRole($role, $user)) {
        return true;
      }
    }
  }

  protected function userHasRole($role, $user) {
    if ($role === '?') {
      return !$user OR !$user->getIdentity();
    } elseif ($role === '@') {
      return $user && $user->getIdentity();
    }
    return $user && $user->identity[$this->roleAttribute] == $role;
  }
}
