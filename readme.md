# Install
```bash
composer config repos.gii vcs https://gitlab.com/yus-ham/yii2-gii.git
composer require yusi/yii2-gii:@dev -o --dev
```

# Usage
```php
$config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],

    'generators' => [
        'model' => [
            'class' => 'Yusi\Gii\Model\Generator',
            'templates' => ['digitak' => '@vendor/yusi/yii2-gii/templates/model'],
        ],
        'crud' => [
            'class' => 'Yusi\Gii\Crud\Generator',
            'templates' => [
                'digitak_gentelella_master' => '@vendor/yusi/yii2-gii/templates/crud/gentelella',
                'digitak_syrena_master' => '@vendor/yusi/yii2-gii/templates/crud/syrena',
                'digitak_inspinia_master' => '@vendor/yusi/yii2-gii/templates/crud/inspinia',
            ],
        ],
    ],
];
```
