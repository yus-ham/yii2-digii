<?php
namespace Yusi\Gii\Crud;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\helpers\StringHelper;
use yii\web\Controller;

/**
 * Generates CRUD
 *
 * @property array $columnNames Model column names. This property is read-only.
 * @property string $controllerID The controller ID (without the module ID prefix). This property is
 * read-only.
 * @property string $nameAttribute This property is read-only.
 * @property array $searchAttributes Searchable attributes. This property is read-only.
 * @property bool|\yii\db\TableSchema $tableSchema This property is read-only.
 * @property string $viewPath The controller view path. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\generators\crud\Generator
{
    public $attributeConfigTemplates = [];
    public $selectedColumns = [];
    public $accessRuleClass = 'common\components\AccessRule';

    public function init() {
      parent::init();
      $this->modelClass && $this->prepareAttributeTemplates();
    }
    
    public function validateNewClass($attribute, $params) {
        $class = ltrim($this->$attribute, '\\');
        if (($pos = strrpos($class, '\\')) === false) {
            $this->addError($attribute, "The class name must contain fully qualified namespace name.");
        } else {
            $ns = substr($class, 0, $pos);
            $path = Yii::getAlias('@' . str_replace('\\', '/', $ns), false);
            if ($path === false) {
                $this->addError($attribute, "The class namespace is invalid: $ns");
            } elseif (!is_dir($path)) {
                // biarkan auto createDirectori aja
                //$this->addError($attribute, "Please make sure the directory containing this class exists: $path");
            }
        }
    }

    protected function prepareAttributeTemplates() {
      $columns = $this->getDetailWidgetColumns();
      foreach ($columns as $column) {
        
      }
    }

    public function rules() {
      return array_merge(parent::rules(), [
        [['selectedColumns','attributeConfigTemplates','accessRuleClass'], 'safe'],
      ]);
    }

    public function afterValidate() {
      parent::afterValidate();
      $this->setAccessRuleClass();
    }

    protected function setAccessRuleClass() {
      try {
        Yii::getAlias('@common');
      } catch (\Exception $ex) {
        $this->accessRuleClass = 'app\components\AccessRule';
      }
    }

    public function defaultTemplate() {
      return Yii::getAlias('@vendor/yiisoft/yii2-gii/src/generators/crud/default');
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            if ($column === 'is_delete') {
                //$hashConditions[] = "'is_delete' => 0,";
                continue;
            }
            switch ($type) {
                case Schema::TYPE_TINYINT:
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "'{$column}' => \$this->{$column},";
                    break;
                default:
                    $likeKeyword = $this->getClassDbDriverName() === 'pgsql' ? 'ilike' : 'like';
                    $likeConditions[] = "->andFilterWhere(['{$likeKeyword}', '{$column}', \$this->{$column}])";                    
                    break;
            }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode("\n" . str_repeat(' ', 12), $hashConditions)
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode("\n" . str_repeat(' ', 12), $likeConditions) . ";\n";
        }

        return $conditions;
    }

    /** @param \yii\db\ColumnSchema $column */
    public function generateDetailViewAttributeConfig($column) {
        $format = $this->generateColumnFormat($column);

        if ($column->name === 'is_active') {
          ?>
            [
              'attribute' => 'is_active',
              'label' => 'Status',
              'format' => 'html',
              'value' => common\components\Helper::statusLabel($model->is_active),
            ],
<?php
        } elseif (in_array($column->name, $this->detailViewSkipAttributes())) {
            echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }

    protected function detailViewSkipAttributes() {
        return $this->getDefaultSkippedColumns();
    }

    public function getDefaultSkippedColumns() {
        return [
          'id',
          'is_delete',
          'is_deleted',
          'create_at',
          'update_at',
          'update_by',
          'create_by',
          'created_at',
          'updated_at',
          'created_by',
          'updated_by',
        ];
    }

    public function getDetailWidgetColumns() {
      $columns = array_diff($this->getColumnNames(), $this->getDefaultSkippedColumns());
      sort($columns);
      return $columns;
    }

    public function getSelectedColumnNames() {
      return [];
    }

    public function isColumnChecked($columnName) {
      if (isset($this->selectedColumns[$columnName])) {
        return $this->selectedColumns[$columnName];
      }
      return !in_array($columnName, $this->getDefaultSkippedColumns());
    }

    /** @param \yii\db\ColumnSchema $column */
    public function generateColumnTemplate($column) {

      if (StringHelper::endsWith($column->name, '_id')) {
        $label = Inflector::titleize($column->name);
        $getter = 'get'. Inflector::classify($label);

        return "["
            .PHP_EOL."  'attribute' => '{$column->name}',"
            .PHP_EOL."  'value' => function(\$model) use(\$searchModel) {"
            .PHP_EOL."    \$searchModel->{$column->name} = \$model->{$column->name};"
            .PHP_EOL."    return \$searchModel->$getter();"
            .PHP_EOL."  },"
            .PHP_EOL."]";
      }

      if ($column->name === 'is_active') {
        return "["
            .PHP_EOL."  'attribute' => 'is_active',"
            .PHP_EOL."  'label' => 'Status',"
            .PHP_EOL."  'filter' => ['Inactive','Active'],"
            .PHP_EOL."  'format' => 'html',"
            .PHP_EOL."  'value' => function(\$model) {"
            .PHP_EOL."    \$status = [['danger','Inactive'], ['success','Active']][\$model->is_active];"
            .PHP_EOL."    return \"<span class='label label-{\$status[0]}'>{\$status[1]}</span>\";"
            .PHP_EOL."  },"
            .PHP_EOL."]";
      }

      if (in_array($column->type, ['date','time','datetime'])) {
        return "'{$column->name}:{$column->type}'";
      }

      $format = $this->generateColumnFormat($column);
      return "'{$column->name}:$format'";
    }

    public function generateActiveField($attribute) {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput(['value'=>''])";
            }

            return "\$form->field(\$model, '$attribute')";
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->phpType === 'boolean' OR StringHelper::startsWith($attribute, 'is_')) {
            return "\$form->field(\$model, '$attribute')->checkbox()";
        }
        if (StringHelper::endsWith($attribute, '_id')) {
            $relationClass = Inflector::classify(substr($attribute, 0, -3));
            return      "// \$form->field(\$model, '$attribute')->widget(Select2::class, ["
              .PHP_EOL. "//         'data' => ArrayHelper::map("
              .PHP_EOL. "//              $relationClass::find()->where(['is_delete'=>0, 'is_active'=>1])->all(),"
              .PHP_EOL. "//              'id', 'label'),"
              .PHP_EOL. "//         'language' => 'en',"
              .PHP_EOL. "//         'options' => ['placeholder' => '-Pilih-'],"
              .PHP_EOL. "//         'pluginOptions' => ["
              .PHP_EOL. "//             'allowClear' => true,"
              .PHP_EOL. "//         ],"
              .PHP_EOL. "//    ])";
        }
        if ($column->type === 'text') {
            return      "\$form->field(\$model, '$attribute')->widget(TinyMce::class, ["
              .PHP_EOL. "        'options' => ['rows' => 10],"
              .PHP_EOL. "        'language' => 'en',"
              .PHP_EOL. "        'clientOptions' => ["
              .PHP_EOL. "            'plugins' => ["
              .PHP_EOL. "                'advlist autolink lists link charmap print preview anchor',"
              .PHP_EOL. "                'searchreplace visualblocks code fullscreen',"
              .PHP_EOL. "                'insertdatetime media table contextmenu paste',"
              .PHP_EOL. "            ],"
              .PHP_EOL. "            'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',"
              .PHP_EOL. "        ],"
              .PHP_EOL. "])";
        }

        return parent::generateActiveField($attribute);
    }

    public function hasColumn($attr) {
      return $tableSchema = $this->getTableSchema() and isset($tableSchema->columns[$attr]);
    }

    public function hasColumnType($type) {
        if ($tableSchema = $this->getTableSchema()) {
          foreach($tableSchema->columns as $columnName => $column) {
            if ($type === 'FK' && StringHelper::endsWith($columnName, '_id')) {
              return true;
            }
            if ($column->type === $type) {
              return true;
            }
          }
        }
    }

    public function generate(): array {
        $files = parent::generate();
        $files[] = $this->getAccessRuleFile();

        return $files;
    }

    protected function getAccessRuleFile() {
        list($namespace) = explode('\\', $this->accessRuleClass,2);

        $path = Yii::getAlias("@$namespace/components/AccessRule.php");
        $file = new CodeFile($path, $this->render('../../AccessRule.php', ['namespace' => $namespace]));

        return $file;
    }

}
