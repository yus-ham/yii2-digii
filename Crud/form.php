<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator Digitak\Gii\Crud\Generator */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

include Yii::getAlias('@vendor/yiisoft/yii2-gii/src/generators/crud/form.php');

if ($generator->modelClass) {
  echo '<div class="form-group attribute-config-templates"><label class="help" data-original-title="" title="">Index Widget Attributes Configs</label></div>';

  $checked = $generator->isColumnChecked($columnName = '_yii_serial_column_');
  if (empty($generator->attributeConfigTemplates[$columnName])) {
    $generator->attributeConfigTemplates[$columnName] = "['class' => 'yii\grid\SerialColumn']";
  }
  echo '<div class=row style="margin-bottom:2px;padding-left:15px">'
       .Html::checkbox("Generator[selectedColumns][$columnName]", $checked, ['label'=>$columnName, 'labelOptions'=>['class'=>'col-sm-4'], 'uncheck'=>0]).
       '<div class=col-sm-8>'. Html::activeTextarea($generator, "attributeConfigTemplates[$columnName]", ['rows'=>1, 'class'=>'form-control', 'style'=>'padding:2px 5px']) .'</div></div>';

  if (($tableSchema = $generator->getTableSchema()) === false) {
  } else {
    foreach ($tableSchema->columns as $column) {
      $columnName = $column->name;

      if (empty($generator->attributeConfigTemplates[$columnName])) {
        $generator->attributeConfigTemplates[$columnName] = $generator->generateColumnTemplate($column);
      }
      
      $checked = $generator->isColumnChecked($columnName);
      echo '<div class=row style="margin-bottom:2px;padding-left:15px">'.
            Html::checkbox("Generator[selectedColumns][$columnName]", $checked, ['label'=>$columnName, 'labelOptions'=>['class'=>'col-sm-4'], 'uncheck'=>0]).
           '<div class=col-sm-8>'. Html::activeTextarea($generator, "attributeConfigTemplates[$columnName]", ['rows'=>1, 'class'=>'form-control', 'style'=>'padding:2px 5px']) .'</div></div>';
    }
  }

  $checked = $generator->isColumnChecked($columnName = '_yii_action_column_');
  if (empty($generator->attributeConfigTemplates[$columnName])) {
    $generator->attributeConfigTemplates[$columnName] = "[\n  'class' => 'yii\grid\ActionColumn',\n  'header' => 'Aksi',\n  'contentOptions' => ['style'=>'width:80px;text-align:center'],\n]";
  }
  echo '<div class=row style="margin-bottom:2px;padding-left:15px">'
       .Html::checkbox("Generator[selectedColumns][$columnName]", $checked, ['label'=>$columnName, 'labelOptions'=>['class'=>'col-sm-4'], 'uncheck'=>0]).
       '<div class=col-sm-8>'. Html::activeTextarea($generator, "attributeConfigTemplates[$columnName]", ['rows'=>1, 'class'=>'form-control', 'style'=>'padding:2px 5px']) .'</div></div>';
}

$this->registerJs("
  document.getElementById('form-fields').setAttribute('class','col-sm-12')
  var textareas = document.getElementsByTagName('textarea');
  var count = textareas.length;
  for(var i=0;i<count;i++){
    textareas[i].onkeydown = function(e){
      if (e.keyCode==9 || e.which==9){
        e.preventDefault();
        var s = this.selectionStart;
        this.value = this.value.substring(0,this.selectionStart) + '  ' + this.value.substring(this.selectionEnd);
        this.selectionEnd = s+2;
      }
    }
  }
");
